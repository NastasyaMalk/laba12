import { Component, OnInit } from '@angular/core';
import { Person } from './shared/person.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'project3';
  persons: Person[] = [];
  ngOnInit() {
    this.persons.push(new Person(1, 'Иван', 'Иванов'));
    this.persons.push(new Person(2, 'Василий', 'Васильев'));
    this.persons.push(new Person(3, 'Петр', 'Петров'));
    this.persons.push(new Person(4, 'Павел', 'Павлов'));
    this.persons.push(new Person(5, 'Михаил', 'Михайлов'));
    this.persons.push(new Person(6, 'Александр', 'Александров'));
    this.persons.push(new Person(7, 'Роман', 'Романов'));
    this.persons.push(new Person(8, 'Алексей', 'Алексеев'));
    this.persons.push(new Person(9, 'Кирилл', 'Кириллов'));
    this.persons.push(new Person(10, 'Олег', 'Олегов'));

  }
  changePersons(person){
    this.persons.splice(this.persons.findIndex(human => human.id == person.id), 1, person);
  }
  deletePersons(id){
    this.persons.splice(this.persons.findIndex(human => human.id == id), 1);
  }
  addPersons(obj){
    let newId;
    if (this.persons.length!=0){
      newId = +this.persons[this.persons.length-1].id +1;
     }
     else {newId=1;}
    this.persons.push(new Person(newId, obj.name_man, obj.surname_man));
  }
}
